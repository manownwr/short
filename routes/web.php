<?php

Route::get('/','TodosController@index');
Route::get('/t/{short}', 'TodosController@show');
Route::resource('/','TodosController');

